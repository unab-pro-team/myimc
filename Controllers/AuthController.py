# coding=utf8
import sys
from Controllers.Controller import *

class AuthController(Controller):
    def __init__(self, User):
        super(AuthController, self).__init__(User)

    
    def index(self):
        '''
        Muestra la vista de llegada.
        '''
        return view('auth-index')
    
    def logout(self):
        print('Se cierra la session.')
        sys.exit()

    def login(self, email=None):
        '''
        Muestra la vista de login
        '''
        return view('auth-login', {'email': email})
    
    def attemptLogin(self, email, password):
        '''
        Hace el intento de autenticar a un usuario.
        '''
        errors = []
        email = email.strip()
        password = md5(password.encode()).hexdigest()

        user = self._User.getByEmail(email)

        if not user or password != user.password:
            errors.append('Las credenciales no coinciden con ningún registro en la base de datos.')
        
        if len(errors):
            return view('user-validationErrors', {
                'back': 'auth.login',
                'backData': {'email': email},
                'errors': errors
            })
        else:
            self._User.auth().setUser(user)
            return ('user.home',)

    def register(self):
        '''
        Muestra la vista de registro
        '''
        return view('auth-register')
    
    def refreshAuth(self, back=None, backData={}):
        '''
        Refresca los datos de la instancia Auth.
        '''
        id = self._User.auth().getId()
        user = self._User.find(id)
        self._User.auth().setUser(user)
        return (back, backData)
