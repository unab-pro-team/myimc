# coding=utf8
import re
import datetime
from Controllers.Controller import *

class UserController(Controller):
    def __init__(self, User, Measure):
        super(UserController, self).__init__(User)
        self._Measure = Measure

    def home(self):
        '''
        muestra la vista home
        '''
        user = self._User.auth().getUser()
        return view('user-home', {'user': user})
    
    def store(self, email, password):
        '''
        Inserta el registro de usuario.
        '''
        errors = []
        email = email.strip()
        expresion_regular = r"(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])"
        email_valido = re.match(expresion_regular,email) is not None
        if not email_valido:
            errors.append('El email no tiene un formato valido.')

        password = md5(password.encode()).hexdigest()
        user = self._User.getByEmail(email)
        
        if user:
            errors.append('El email ya está registrado.')
        
        if len(errors):
            return view('user-validationErrors', {
                'back': 'auth.register',
                'errors': errors
            })
        else:
            self._User.insert({'email': email, 'password': password})
            return ('auth.index',)

    def edit(
        self, nombre=None, apellido=None, fecha_nacimiento=None,
        sexo=None, tipo=None
    ):
        '''
        muestra vista con formulario de registro de datos personales de usuario.
        '''
        return view('user-edit', {
            'nombre': nombre,
            'apellido': apellido,
            'fecha_nacimiento': fecha_nacimiento,
            'sexo': sexo,
            'tipo': tipo
        })
    
    def update(self, nombre, apellido, fecha_nacimiento, sexo, tipo):
        '''
        Actualiza los datos personales del usuario.
        '''
        errors = []
        nombre = nombre.strip()
        apellido = apellido.strip()
        data = {
            'nombre': nombre,
            'apellido': apellido,
            'fecha_nacimiento': fecha_nacimiento,
            'sexo': sexo,
            'tipo': tipo
        }
        if not nombre or not apellido:
            errors.append('Todos los campos son requeridos.')
            data.pop('nombre', None)
            data.pop('apellido', None)
        try:
            datetime.datetime.strptime(fecha_nacimiento, '%Y-%m-%d')
        except:
            errors.append('La fecha de nacimiento es inválida')
            data.pop('fecha_nacimiento', None)
        if len(errors):
            return view('user-validationErrors', {
                'back': 'user.edit',
                'backData': data,
                'errors': errors
            })

        data['id'] = self._User.auth().getId()
        self._User.update(data)
        return ('auth.refreshAuth',)
    
    def newMeasure(self):
        '''
        muestra la vista newMeasure
        '''
        return view('user-newMeasure')

    def storeMeasure(self, peso, altura, fecha):
        '''
        Inserta una nueva medición del usuario.
        '''
        errors = [];
        if type(peso) != float or peso <= 0:
            errors.append('El valor de peso es inválido.')
        if type(altura) != float or altura <= 0:
            erros.append('El valor de altura es inválido.')
        try: datetime.datetime.strptime(fecha, '%Y-%m-%d')
        except: errors.append('La fecha es inválida.')
        if len(errors):
            return view('user-validationErrors', {
                'back': 'user.newMeasure',
                'errors': errors
            })
        user = self._User.auth().getUser()
        imc = self._getIMC(peso, altura)
        estado_nut = self._getEstadoNut(user.sexo, imc)
        data = {
            'user_id': user.id,
            'fecha': fecha,
            'peso': peso,
            'altura': altura,
            'imc': imc,
            'estado_nut': estado_nut
        }
        self._Measure.insert(data)
        return ('user.listMeasure',)

    def listMeasure(self):
        '''
        Muestra la vista de historial de mediciones.
        '''
        user_id = self._User.auth().getId()
        return view('user-listMeasure', {
            'measures': self._Measure.getByUser(user_id)
        })

    def _getIMC(self, peso, altura):
        '''
        retorna cálculo del imc
        '''
        return peso/(altura ** 2)
    
    def _getEstadoNut(self, sexo, imc):
        '''
        retorna el estado nutricional
        '''
        if imc < 20.05: estado = 'BAJO PESO'
        elif sexo == 'M':
            if imc < 24.95: estado = 'NORMAL'
            elif imc >= 24.95 and imc < 29.95: estado = 'OBESIDAD LEVE'
            elif imc >= 29.95 and imc < 40.05: estado = 'OBESIDAD SEVERA'
            else: 'OBESIDAD MUY SEVERA'
        else:
            if imc < 23.95: estado = 'NORMAL'
            elif imc >= 23.95 and imc < 28.95: estado = 'OBESIDAD LEVE'
            elif imc >= 28.95 and imc < 37.05: estado = 'OBESIDAD SEVERA'
            else: 'OBESIDAD MUY SEVERA'
        return estado