# coding=utf8
import sys
import conf as c

def route(name, **kwargs):
    if name == None or name == 'user.home':
        return c.gUserController.home(**kwargs)

    if name == 'auth.index':
        return c.gAuthController.index(**kwargs)

    if name == 'auth.logout':
        return c.gAuthController.logout(**kwargs)

    if name == 'auth.login':
        return c.gAuthController.login(**kwargs)

    if name == 'auth.attemptLogin':
        return c.gAuthController.attemptLogin(**kwargs)
    
    if name == 'auth.register':
        return c.gAuthController.register(**kwargs)
    
    if name == 'auth.refreshAuth':
        return c.gAuthController.refreshAuth(**kwargs)
    
    if name == 'user.store':
        return c.gUserController.store(**kwargs)

    if name == 'user.edit':
        return c.gUserController.edit(**kwargs)
    
    if name == 'user.update':
        return c.gUserController.update(**kwargs)
    
    if name == 'user.newMeasure':
        return c.gUserController.newMeasure(**kwargs)
    
    if name == 'user.storeMeasure':
        return c.gUserController.storeMeasure(**kwargs)
    
    if name == 'user.listMeasure':
        return c.gUserController.listMeasure(**kwargs)
    
    # Rutear arriba =================================================
    sys.exit('Ruta no encontrada ({})'.format(name))