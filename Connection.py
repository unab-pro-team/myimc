# coding=utf8
import mariadb
import sys
from os import listdir, getenv
from os.path import isfile, join
from dotenv import load_dotenv
from time import sleep

class Connection:
    '''
    Crea base de datos y ejecuta scripts del directorio database.
    '''
    _conn = None
    _tries = None

    def __init__(self, tries=3):
        self._tries = tries
        load_dotenv()
        self.DB_USER = getenv('DB_USER')
        self.DB_PASSWORD = getenv('DB_PASSWORD')
        self.DB_HOST = getenv('DB_HOST')
        self.DB_PORT = int(getenv('DB_PORT'))
        self.DB_DATABASE = getenv('DB_DATABASE')

    def __del__(self):
        # cerrar conexión si el objeto es destruido
        if self._conn: self._conn.close()

    def _createDB(self):
        '''
        Crea la base de datos si no existe.
        '''
        conn = None
        try:
            conn = mariadb.connect(
                user = self.DB_USER,
                password = self.DB_PASSWORD,
                host = self.DB_HOST,
                port = self.DB_PORT
            )
            mycursor = conn.cursor()
            mycursor.execute(f'CREATE DATABASE IF NOT EXISTS {self.DB_DATABASE}')
        finally:
            if conn: conn.close()
    
    def _checkConn(self):
        '''
        Consulta si existe la tabla users y además se puede usar para checkear si la conexión está activa.
        '''
        cursor = self._conn.cursor()
        cursor.execute("show tables like 'users'")
        if cursor.fetchone():
            return True
        return False
    
    def _checkDB(self):
        '''
        Chechea que exista la BD con sus tablas. Las crea en caso contrario.
        '''
        print('Comprobando conexión a la base de datos...')
        conn = None
        try:
            self._createDB()

            self._conn = mariadb.connect(
                user = self.DB_USER,
                password = self.DB_PASSWORD,
                host = self.DB_HOST,
                port = self.DB_PORT,
                database = self.DB_DATABASE,
                autocommit=True
            )
        except mariadb.Error as e:
            # si hay error, el sistema no puede continuar
            print(f"Error al conectar a MariaDB: {e}")
            sys.exit(1)
        
        print('Comprobando la base de datos...')
        if self._checkConn():
            return True
        else:
            self._migrate()
            return True
        return False
    
    def _migrate(self):
        '''
        Corre los scripts sql del directorio database
        '''
        migPath = './database/'
        migrations = [f for f in listdir(migPath) if isfile(join(migPath, f))]
        migrations.sort()

        fPaths = []
        for f in migrations:
            fPaths.append(migPath + f)

        print('Corriendo migraciones...')
        cursor = self._conn.cursor()
        for fPath in fPaths:
            print(fPath)
            f = open(fPath)
            querys = "".join([line for line in f.readlines()]) \
                .replace('\n', ' ').split(';')
            querys = map(lambda i: i.strip(), querys)
            querys = list(filter(lambda i: i != '', querys))
            for q in querys:
                cursor.execute(q)
    
    def get(self):
        '''
        Retorna la conexión activa a la base de datos.
        '''
        # primera vez, checkea si es posible conectar a la base de datos y la crea si no existe.
        if not self._conn:
            self._checkDB()
        else:
            tries = 0
            success = False
            while not success:
                try:
                    self._checkConn()
                    success = True
                except mariadb.Error as e:
                    if tries < self._tries:
                        print('Se ha perdido la conexión. Intentando reconectar...')
                        tries += 1
                        sleep(5)
                    else:
                        print(f"Error al conectar a MariaDB: {e}")
                        sys.exit(1)

        return self._conn
