# coding=utf8
import datetime
from views import view

class View:
    def title(self, t):
        '''
        Estilo título
        '''
        print('{:=^50}'.format(' {} '.format(t)))

    def _askForOption(self, options):
        '''
        Pregunta por una opción enumerada. Retorna la respuesta del usuario.
        '''
        print('Seleccione una opción:')
        for k, v in enumerate(options):
            print('{}. {}'.format(k+1, v))
        return input('>> ')

    def _confirmInput(self, label, data, default=False):
        '''
        Pregunta si se desea cambiar el valor de un input y retorna el valor o
        nuevo valor según la respuesta.
        '''
        print('{}: {}'.format(label, data))
        options = 'S/n' if default else 's/N'
        answer = input('Cambiar {}? [{}]:'.format(label.lower, options))
        options = ['n', 'no'] if default else ['s', 'si', 'y', 'yes']
        if answer.lower() in options:
                data = input('{}: '.format(label))
        return data
    
    def _from_dob_to_age(self, born):
        '''
        De fecha de nacimiento a edad.
        '''
        today = datetime.date.today()
        return today.year - born.year - (
            (today.month, today.day) < (born.month, born.day)
        )
    
    def _askForDate(self, min=None, max=None):
        '''
        Pregunta por una fecha entre min y max. Retorna string fecha ingresada en formato Y-m-d
        '''
        min = self._dateYmd(min) if min else self._dateYmd('1900-01-01')
        max = self._dateYmd(max) if max else datetime.date.today()
        fecha = None
        year = input('Año: ')
        while not year or not year.isnumeric() \
            or int(year) < min.year or int(year) > max.year:
            print('Ingrese un año válido:')
            year = input('Año: ')
        month = input('Mes: ')
        while not month or not month.isnumeric() \
            or int(month) < 1 or int(month) > 12 \
            or self._dateYmd('{}-{}-01'.format(min.year, min.month)) > \
            self._dateYmd(year + '-' + month + '-01') \
            or self._dateYmd('{}-{}-01'.format(max.year, max.month)) < \
            self._dateYmd(year + '-' + month + '-01') :
            print('Ingrese un mes válido:')
            month = input('Mes: ')
        day = input('Día: ')
        while True:
            fecha = '{}-{}-{}'.format(year, month, day)
            try:
                datetime.datetime.strptime(fecha, '%Y-%m-%d')
            except:
                fecha = None
            if not fecha \
                or min > self._dateYmd(fecha) \
                or max < self._dateYmd(fecha):
                print('Ingrese un día válido:')
                day = input('Día: ')
            else:
                break
        return fecha

    def _dateYmd(self, Ymd):
        '''
        Retorna date a partir de un string en formato %Y-%m-%d o %Y%m%d
        '''
        try:
            return datetime.datetime.strptime(Ymd, '%Y-%m-%d').date()
        except:
            return datetime.datetime.strptime(Ymd, '%Y%m%d').date()