# coding=utf8
from Views.View import *
from tabulate import tabulate
from sty import fg, rs

class UserView(View):
    def validationErrors(self, errors, back, backData={}):
        '''
        Muestra mensajes de error al validar formularios. Redirige a la ruta indicada en back, con backData
        '''
        self.title('Errores de validación')
        for e in errors:
            print('* {}'.format(e))
        return (back, backData)
    
    def home(self, user):
        '''
        Home del usuario.
        '''
        self.title('Bienvenido ' + user.email)
        if None not in (user.fecha_nacimiento, user.sexo):
            print(tabulate([
                ['Nombre:', '{} {}'.format(user.nombre, user.apellido)],
                ['Edad:', self._from_dob_to_age(user.fecha_nacimiento)],
                ['Sexo:', 'masculino' if user.sexo == 'M' else 'femenino'],
                ['Atleta:', 'sí' if user.tipo == 'A' else 'no']
            ]))
        else:
            print('Antes de revisar su índice de masa corporal, por favor registre sus datos personales.')
            return ('user.edit',)
        option = self._askForOption([
            'Ingresar nueva medición.',
            'Ver historial de mediciones.',
            'Logout.'
        ])
        if option == '1': return ('user.newMeasure',)
        elif option == '2': return ('user.listMeasure',)
        elif option == '3': return ('auth.logout',)
        else:
            print('Opción inválida.')
            return ('user.home',)

    def edit(
        self, nombre=None, apellido=None, fecha_nacimiento=None,
        sexo=None, tipo=None
    ):
        '''
        Formulario de ingreso de datos personales.
        '''
        self.title('Ingreso de datos personales.')
        if nombre:
            nombre = self._confirmInput('Nombre', nombre)
        else:
            nombre = input('Nombre: ')
        if apellido:
            apellido = self._confirmInput('Apellidos', apellido)
        else:
            apellido = input('Apellidos: ')
        if fecha_nacimiento:
            print('Fecha de nacimiento: ' + fecha_nacimiento)
            answer = input('Cambiar fecha? s/N:')
            if answer.lower() in ['s', 'si', 'y', 'yes']:
                fecha_nacimiento = None  
        if not fecha_nacimiento:
            print('Ingrese su fecha de nacimiento:')
            fecha_nacimiento = self._askForDate()
        print('Indique su sexo.')
        option = self._askForOption([
            'masculino',
            'femenino'
        ])
        sexo = 'M' if option == '1' else 'F'
        answer = input('Indique si es atleta. s/N: ')
        tipo = 'A' if answer.lower() in ['s', 'si', 'y', 'yes'] else 'N'

        return ('user.update', {
            'nombre': nombre,
            'apellido': apellido,
            'fecha_nacimiento': fecha_nacimiento,
            'sexo': sexo,
            'tipo': tipo
        })

    def newMeasure(self):
        self.title('Nueva medición.')
        while True:
            peso = input('Peso: ')
            try:
                peso = float(peso)
                break
            except:
                print("Ingrese un peso válido:")
        while True:
            altura = input('Altura: ')
            try:
                altura = float(altura)
                break
            except:
                print("Ingrese una altura válida:")
        while True:
            #pedir fecha de medicion
            #opciones, opcion usar la fecha de hoy S/n 
            answer = input('¿Desea que la medición tenga la fecha de hoy? S/n:')
            if answer.lower() in ['s', 'si', 'y', 'yes'] or answer=='':
                fecha_medicion = datetime.date.today()
                fecha_medicion = '{}-{}-{}'.format( fecha_medicion.year,fecha_medicion.month,fecha_medicion.day)
                break
            elif answer.lower() in ['n', 'no'] :    
                fecha_medicion = self._askForDate()
                break
            else:
                print('Ingrese una respuesta válida')
        
        
        #responde Si obtener fecha de hoy y convertir en String en formato (y-m-d)
        #Si responde No usar la funcion self._askForDate()
        return ("user.storeMeasure", {
            "peso": peso,
            "altura": altura,
            "fecha": fecha_medicion
        })
    
    def listMeasure(self, measures):
        '''
        Muestra el historial de mediciones
        '''
        self.title('Historial de mediciones de IMC.')
        data = []
        for m in measures:
            data.append([
                m.fecha.strftime("%d-%m-%Y"),
                m.peso,
                m.altura,
                m.imc,
                self._estadoWithColor(m.estado_nut)
            ])

        print(tabulate(data, headers=[
            'Fecha', 'Peso', 'Altura', 'IMC', 'Estado Nutricional'
        ]))
        input('\nPresione Enter para continuar...')
        return ('user.home',)
    
    def _estadoWithColor(self, estado):
        if estado == 'BAJO PESO': estado = fg.li_yellow + estado + fg.rs
        elif estado == 'NORMAL': estado = fg.li_green + estado + fg.rs
        elif estado == 'OBESIDAD LEVE': estado = fg(255,120,50) + estado + fg.rs
        else: estado = fg.red + estado + fg.rs
        return estado
