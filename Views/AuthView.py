# coding=utf8
from Views.View import *
from getpass import getpass

class AuthView(View):
    def index(self):
        '''
        Vista inicial, redirige a la opción elegida.
        '''
        self.title('Bienvenido a MyIMC')
        print('Seleccione una opción:')
        print('1. Login')
        print('2. Registrarse')
        option = input('>> ')
        if option == '1': return ('auth.login',)
        elif option == '2': return ('auth.register',)
        return ('auth.logout',)

    def login(self, email=None):
        '''
        Formulario para autenticar a un usuario
        '''
        self.title('Login')
        if email:
            answer =  input('Reintentar? S/n: ')
            if answer.lower()  in ['n', 'no']: return ('auth.index',)
            print('Email: ' + email)
            answer =  input('Cambiar email? s/N: ')
            if answer.lower() in ['s', 'si', 'y', 'yes']:
                email = input('Email: ')
        else:
            email = input('Email: ')
        password = getpass()
        return ('auth.attemptLogin', {'email': email, 'password': password})

    def register(self, email=None):
        '''
        Formulario para registrar a un usuario
        '''
        self.title('Register')
        if email:
            answer =  input('Reintentar? S/n: ')
            if answer.lower()  in ['n', 'no']: return ('auth.index',)
            print('Email: ' + email)
            answer =  input('Cambiar email? s/N: ')
            if answer.lower() in ['s', 'si', 'y', 'yes']:
                email = input('Email: ')
        else:
            email = input('Email: ')
        password = getpass()
        return ('user.store', {'email': email, 'password': password})