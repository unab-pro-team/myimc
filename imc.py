# coding=utf8
import conf as c
from routes import route

if __name__ == "__main__":
    c.init()
    # iniciar conexión a la bd
    c.gConn.get()

    # motor
    request = route('auth.index') # arranque
    while 1:
        # print(request) # debug
        if not request or len(request) == 1:
            if not request: request = (None,)
            request = (request[0], {})
        request = route(request[0], **request[1])