# coding=utf8
from Connection import Connection
from Auth import Auth
from Controllers.AuthController import AuthController
from Controllers.UserController import UserController
from Models.Measure import Measure
from Models.User import User
from Views.AuthView import AuthView
from Views.UserView import UserView

def init():
    global gConn
    global gUser, gMeasure
    global gAuthController, gUserController
    global gAuthView, gUserView

    gConn = Connection()
    gAuth = Auth()
    gUser = User(gConn, gAuth)
    gMeasure = Measure(gConn)
    gAuthController = AuthController(gUser)
    gUserController = UserController(gUser, gMeasure)
    gAuthView = AuthView()
    gUserView = UserView()