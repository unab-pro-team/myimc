# MyIMC

## Instalación

* Instalar Python v3.8 o mayor
* Instalar XAMPP https://www.apachefriends.org/download.html
* Instalar https://mariadb.com/docs/clients/connector-c/*connc-install
* Iniciar servicio mysql(mariadb) en XAMPP
* Copiar .env.example al nuevo archivo .env y poner credenciales de la base de
datos. El ejemplo incluye las por defecto en XAMPP.

* Instalar los siguientes paquetes de python usando pip en línea de comandos:
```bash
# usar pip3 o simplemente pip, según su configuración.

# https://mariadb.com/docs/clients/connector-python/
pip3 install -U mariadb

# Reads key-value pairs from a .env file.
pip3 install -U python-dotenv

# Pretty-print tabular data in Python.
pip3 install -U tabulate

# String styling for your terminal.
pip3 install -U sty
```

## Ejecución

En una terminal ingresar a la carpeta de la aplicación y ejecutar:
```bash
# usar python3 o simplemente python según su configuración.
python3 imc.py
```