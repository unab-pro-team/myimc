# coding=utf8

class Auth:
    _user = None

    def getId(self):
        (id, *_) = self._user
        return id

    def getUser(self):
        return self._user
    
    def setUser(self, user):
        self._user = user
