# coding=utf8
from Models.Model import *

class User(Model):
    def __init__(self, Connection, Auth):
        super(User, self).__init__(Connection)
        self._Auth = Auth

    def auth(self):
        '''
        Retorna el objeto Auth para trabajar con el usuario logeado
        '''
        return self._Auth
    
    def find(self, id):
        '''
        Busca un usuario por id
        '''
        conn = self._Connection.get()
        cursor = conn.cursor(named_tuple=True)
        cursor.execute('select * from users where id = ?', (id,))
        return cursor.fetchone()

    def getByEmail(self, email):
        '''
        Busca un usuario donde el coincida email
        '''
        conn = self._Connection.get()
        cursor = conn.cursor(named_tuple=True)
        cursor.execute('select * from users where email = ?', (email,))
        return cursor.fetchone()
    
    def insert(self, data):
        '''
        Inserta un nuevo usuario
        '''
        conn = self._Connection.get()
        cursor = conn.cursor()
        # Al registrar solo se incluye email y password
        cursor.execute("""
            INSERT INTO
                users(email, password)
            VALUES (?, ?)
        """, (data['email'], data['password']))

        return cursor.lastrowid # id recien creado

    def update(self, data):
        '''
        Actualiza los datos personales del usuario
        '''
        id = data.pop('id', None)
        data = (
            data['nombre'], data['apellido'], data['fecha_nacimiento'],
            data['sexo'], data['tipo'], id
        )
        conn = self._Connection.get()
        cursor = conn.cursor()
        cursor.execute("""
            UPDATE users
            SET nombre=?,
                apellido=?,
                fecha_nacimiento=?,
                sexo=?,
                tipo=?
            WHERE id=?
        """, data)