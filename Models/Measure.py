# coding=utf8
from Models.Model import *

class Measure(Model):
    def __init__(self, Connection):
        super(Measure, self).__init__(Connection)
    
    def insert(self, data):
        '''
        Inserta una nueva medida.
        '''
        conn = self._Connection.get()
        cursor = conn.cursor()
        cursor.execute("""
            INSERT INTO
                measures(user_id, fecha, peso, altura, imc, estado_nut)
            VALUES (?, ?, ?, ?, ?, ?)
        """, (
            data['user_id'],
            data['fecha'],
            data['peso'],
            data['altura'],
            data['imc'],
            data['estado_nut']
        ))

        return cursor.lastrowid # id recien creado
    
    def getByUser(self, user_id):
        '''
        Inserta una nueva medida.
        '''
        conn = self._Connection.get()
        cursor = conn.cursor(named_tuple=True)
        cursor.execute("""
            SELECT * FROM measures
            WHERE user_id = ?
            ORDER BY fecha DESC
        """, (user_id,))
        return cursor.fetchall()