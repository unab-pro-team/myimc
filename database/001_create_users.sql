create table users(
    id int unsigned auto_increment primary key,
    email varchar(128) not null,
    password varchar(32) not null,
    tipo varchar(1),
    nombre varchar(128),
    apellido varchar(128),
    sexo varchar(1),
    fecha_nacimiento date,
    unique key unique_email (email)
) ENGINE=INNODB
character set utf8
collate utf8_unicode_ci;