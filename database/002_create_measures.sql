create table measures (
    id int unsigned auto_increment primary key,
    user_id int unsigned not null,
    fecha timestamp not null default current_timestamp,
    peso decimal(5,2) not null,
    altura decimal(5,2) not null,
    imc decimal(5,2) not null,
    estado_nut varchar(32) not null,
    foreign key (user_id)
        references users (id)
) ENGINE=INNODB
character set utf8
collate utf8_unicode_ci;