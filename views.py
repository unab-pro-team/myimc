# coding=utf8
import sys
import conf as c

def view(name, kwargs={}):
    if name == 'auth-index':
        return c.gAuthView.index(**kwargs)

    if name == 'auth-login':
        return c.gAuthView.login(**kwargs)
    
    if name == 'auth-register':
        return c.gAuthView.register(**kwargs)
    
    if name == 'user-validationErrors':
        return c.gUserView.validationErrors(**kwargs)

    if name == 'user-home':
        return c.gUserView.home(**kwargs)
    
    if name == 'user-edit':
        return c.gUserView.edit(**kwargs)
    
    if name == 'user-newMeasure':
        return c.gUserView.newMeasure(**kwargs)
    
    if name == 'user-listMeasure':
        return c.gUserView.listMeasure(**kwargs)

    # Rutear vistas arriba ==========================================
    sys.exit('Vista no encontrada ({})'.format(name))
